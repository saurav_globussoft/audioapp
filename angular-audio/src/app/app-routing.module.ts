import { ProfileComponent } from './pages/profile/profile.component';
import { PlayerComponent } from './pages/player/player.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: "", component: PlayerComponent },
  { path: "profile", component: ProfileComponent },
  { path: "**", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
